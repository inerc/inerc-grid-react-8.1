// ag-grid-react v8.1.0
import { IFilterComp } from "ag-grid";
export declare function reactFilterFactory(reactComponent: any, parentComponent?: any): {
    new (): IFilterComp;
};
