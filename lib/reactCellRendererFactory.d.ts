// ag-grid-react v8.1.0
import { ICellRendererComp } from 'ag-grid';
export declare function reactCellRendererFactory(reactComponent: any, parentComponent?: any): {
    new (): ICellRendererComp;
};
