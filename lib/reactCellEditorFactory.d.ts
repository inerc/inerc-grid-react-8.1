// ag-grid-react v8.1.0
import { ICellEditorComp } from 'ag-grid';
export declare function reactCellEditorFactory(reactComponent: any, parentComponent?: any): {
    new (): ICellEditorComp;
};
